//题目分析:
//假如原来是三进制的话，权值是0 1 2，现在是-1 0 1，此时表示2的值为1-
//所以可以先算出来原3进制，然后将2替换成1-，再相加即可。
//什么相加？例如8三进制为22，换成1- + 1- 位置要对好，竖式学过吧。得到 10-。
//如果还不懂的话友情啰嗦下，- + 1 = 0， 0 + 1 = 1， 1 + 1 = - 并且向前进1
//还有负数怎么办？如果你有列写下，会发现规律的，
//那就是同一个数的正负相加为0，即1→-， -→1， 0保持

//题目网址:http://soj.me/1298

#include <iostream>

using namespace std;

bool negative = false;                       //判断输入是否为负数

int theRemaider(int tn, int rem[])
{
    int k = 32;
    while(true) {
        if(tn/3 != 0) {
            rem[--k] = tn % 3;
            tn /= 3;
        } else {
            rem[--k] = tn % 3;
            break;
        }
    }

    return k;
}

void exchange(char theNew[], int rem[], int &k)
{
    for (int i = 31; i >= k; i--) {
        theNew[i] = char(rem[i]) + '0';
    }

    bool plus = false;                  //进位标志位
    for (int i = 31; i >= k; i--) {
        if (!plus) {
            if (rem[i] == 2) {
                theNew[i] = '-';
                plus = true;
            }
        } else {
            if (rem[i]+1 == 2) {
                theNew[i] = '-';
            } else if (rem[i]+1 == 3) {
                theNew[i] = '0';
            } else {
                theNew[i] = '1';
                plus = false;
            }
        }
    }
    if (plus) {                           //最后判断下最高位有没有发生进位，有要+1哦
        k--;
        theNew[k] = '1';
    }

    for(int j = k; j < 32; j++) {   //输出结果
        if (negative) {
            if (theNew[j] == '1') {
                cout << '-';
            } else if (theNew[j] == '-') {
                cout << '1';
            } else {
                cout << '0';
            }
        } else {
            cout << theNew[j];
        }
    }
    negative = false;
    cout << endl;

    return;
}

int main()
{
    int ten;                                                   //输入的十位数
    int remainder[32] = {0};                       //算出三进制
    int theIndexOfRemainder;                  //三进制的最高位所在位置
    char theNewNumber[32] = {'0'};        //新进制

    while(cin >> ten) {
        if (ten < 0) {
            ten = -ten;
            negative = true;
        }
        theIndexOfRemainder = theRemaider(ten, remainder); //算三进制，最高位所在位置
        exchange(theNewNumber, remainder, theIndexOfRemainder);//转换为新进制
    }

    return 0;
}